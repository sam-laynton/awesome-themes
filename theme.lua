-------------------------------
--  "Zenburn" awesome theme  --
--    By Adrian C. (anrxc)   --
-------------------------------

-- Alternative icon sets and widget icons:
--  * http://awesome.naquadah.org/wiki/Nice_Icons

-- {{{ Main
theme = {}
theme.path = "/home/samlay01/.config/awesome/theme/"
theme.wallpaper = "coloured-rings.jpg"
--"zenburn-background.png"
theme.wallpaper_cmd = { "awsetbg -f " .. theme.path .. "wallpaper/" .. theme.wallpaper}
-- }}}

-- {{{ Styles
--theme.font      = "xos4 Terminess Powerline 8"
theme.font  = "Delugia Nerd Font 8"

-- {{{ Colors
S_yellow    = "#b58900"
S_orange    = "#cb4b16"
S_red       = "#dc322f"
S_magenta   = "#d33682"
S_violet    = "#6c71c4"
S_blue      = "#268bd2"
S_cyan      = "#2aa198"
S_green     = "#859900"

S_base03    = "#002b36"
S_base02    = "#073642"
S_base01    = "#586e75"
S_base00    = "#657b83"
S_base0     = "#839496"
S_base1     = "#93a1a1"
S_base2     = "#eee8d5"
S_base3     = "#fdf6e3"

theme.red   = S_red
theme.gre   = S_green
theme.yel   = S_yellow
theme.blu   = S_blue
theme.mag   = S_magenta
theme.cya   = S_cyan
theme.ora   = S_orange
theme.vio   = S_violet

theme.emph = S_base01
theme.body = S_base00
theme.comment = S_base1
theme.backhigh = S_base2
theme.back = S_base3

theme.fg_normal = S_base0
theme.fg_focus  = S_base1
theme.fg_urgent = S_orange
theme.bg_normal = S_base03
theme.bg_focus  = S_base02
theme.bg_urgent = S_base01
-- }}}

-- {{{ Borders
theme.border_width  = "0"
theme.border_normal = "#3F3F3F"
theme.border_focus  = "#6F6F6F"
theme.border_marked = "#CC9393"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3F"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC9393"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = "15"
theme.menu_width  = "100"
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = theme.path .. "taglist/squarefz.png"
theme.taglist_squares_unsel = theme.path .. "taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = theme.path .. "awesome-icon.png"
theme.menu_submenu_icon      = theme.path .. "submenu.png"
theme.tasklist_floating_icon = theme.path .. "tasklist/cloud.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = theme.path .. "layouts/tile.png"
theme.layout_tileleft   = theme.path .. "layouts/tileleft.png"
theme.layout_tilebottom = theme.path .. "layouts/tilebottom.png"
theme.layout_tiletop    = theme.path .. "layouts/tiletop.png"
theme.layout_fairv      = theme.path .. "layouts/fairv.png"
theme.layout_fairh      = theme.path .. "layouts/fairh.png"
theme.layout_spiral     = theme.path .. "layouts/spiral.png"
theme.layout_dwindle    = theme.path .. "layouts/dwindle.png"
theme.layout_max        = theme.path .. "layouts/max.png"
theme.layout_fullscreen = theme.path .. "layouts/fullscreen.png"
theme.layout_magnifier  = theme.path .. "layouts/magnifier.png"
theme.layout_floating   = theme.path .. "layouts/floating.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = theme.path .. "titlebar/close_focus.png"
theme.titlebar_close_button_normal = theme.path .. "titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = theme.path .. "titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = theme.path .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = theme.path .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = theme.path .. "titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = theme.path .. "titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = theme.path .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = theme.path .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = theme.path .. "titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = theme.path .. "titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = theme.path .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = theme.path .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = theme.path .. "titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = theme.path .. "titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = theme.path .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.path .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.path .. "titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
